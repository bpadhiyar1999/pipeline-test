package com.bharat.test.controllers;

import com.bharat.test.models.User;
import com.bharat.test.services.UserProfileService;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.core.oidc.user.DefaultOidcUser;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api/v1")
public class DemoController {

    UserProfileService userProfileService;

    public DemoController(UserProfileService userProfileService) {
        this.userProfileService = userProfileService;
    }

    @GetMapping("hello")
    public String getHome(HttpServletRequest request) {
        DefaultOidcUser principal = (DefaultOidcUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        principal.getIdToken().getEmail();
        return "Hello world";
    }

    @GetMapping("/userProfile")
    public User getUserProfile(@AuthenticationPrincipal OidcUser principal) {
        return userProfileService.fetchByEmail(principal.getEmail());
    }
}
