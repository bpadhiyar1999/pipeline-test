package com.bharat.test.filter;

import com.bharat.test.services.UserProfileService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.jwt.Jwt;
import org.springframework.security.jwt.JwtHelper;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.core.OAuth2AccessToken;
import org.springframework.security.oauth2.core.oidc.OidcIdToken;
import org.springframework.security.oauth2.core.oidc.user.DefaultOidcUser;
import org.springframework.security.oauth2.core.oidc.user.OidcUserAuthority;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.util.StringUtils;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.Instant;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class TokenAuthenticationFilter extends AbstractAuthenticationProcessingFilter {

    private UserProfileService profileService;

    public TokenAuthenticationFilter(String defaultFilterProcessesUrl, UserProfileService profileService) {
        super(defaultFilterProcessesUrl);
        this.profileService = profileService;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws AuthenticationException, IOException, ServletException {
        String token = httpServletRequest.getHeader(HttpHeaders.AUTHORIZATION);
        token  = getBearerToken(token);
        Jwt jwt = JwtHelper.decode(token);
        Map<String, String> claim = new ObjectMapper().readValue(jwt.getClaims(), new TypeReference<Map<String, String>>() {
        });
        OidcIdToken.Builder oidc = OidcIdToken.withTokenValue(token).claims(m -> {
            m.putAll(claim);
        });

        oidc.issuedAt(Instant.ofEpochSecond(Long.parseLong(claim.get("iat"))));
        oidc.expiresAt(Instant.ofEpochSecond(Long.parseLong(claim.get("exp"))));

        OidcIdToken oidcToken = oidc.build();

        boolean exist = profileService.exist(oidcToken.getEmail());
        if (!exist) {
            throw new InternalAuthenticationServiceException("Unauthorized");
        }

        Set<GrantedAuthority> authorities = new HashSet<>();
        authorities.add(new OidcUserAuthority(oidc.build()));

        DefaultOidcUser defaultOidcUser = new DefaultOidcUser(authorities, oidc.build());

        return new OAuth2AuthenticationToken(defaultOidcUser, authorities, "299352296399-kjng616j38slcrrvau6qodl6sto7poj8.apps.googleusercontent.com");
    }

    private String getBearerToken(String token)  {
        if (StringUtils.hasText(token) && token.startsWith(OAuth2AccessToken.TokenType.BEARER.getValue())) {
            token = token.replace(OAuth2AccessToken.TokenType.BEARER.getValue(), "");
            return token;
        }

        throw new InternalAuthenticationServiceException("Token not found");
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {
        SecurityContextHolder.getContext().setAuthentication(authResult);
        chain.doFilter(request, response);
    }

    @Override
    protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response, AuthenticationException failed) throws IOException, ServletException {
        response.sendError(HttpStatus.UNAUTHORIZED.value());
    }
}