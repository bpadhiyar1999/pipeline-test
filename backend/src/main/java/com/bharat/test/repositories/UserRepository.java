package com.bharat.test.repositories;

import com.bharat.test.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    User findByEmailId(String emailId);

    boolean existsByEmailId(String emailId);
    boolean existsUserByEmailIdIgnoreCase(String emailId);
}
