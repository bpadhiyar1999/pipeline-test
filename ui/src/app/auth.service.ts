import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }


  isLoggedIn() {
    console.log('inside logged in service');
    var token = localStorage.getItem('token');
    return token !== null;
  }

  getToken() {
    return localStorage.getItem('token');
  }

  addToken(token: string) {
    localStorage.setItem('token', token);
  }
}