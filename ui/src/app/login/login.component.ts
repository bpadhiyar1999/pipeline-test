import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { AuthService } from '../auth.service';
import { UserService } from '../user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private route:ActivatedRoute, private authServie: AuthService, private router: Router, private userService: UserService) { }

  ngOnInit(): void {
    console.log('Environment : ', environment.workspace);
    const token = this.route.snapshot.queryParamMap.get('token');
    if(token!=null) {
      this.authServie.addToken(token);
      this.router.navigate(['/']);
    }
  }
  doLogin() {
    // this.userService.open();
    window.location.href = `${environment.loginEndpoint}/oauth2/authorization/google`;
  }
}
