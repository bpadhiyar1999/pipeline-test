import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private httpClint : HttpClient) { }

  getProfile() {
    this.httpClint.get('/api/v1/userProfile').subscribe((res) => console.log(res));
  }

  open() {
    this.httpClint.get('https://bharat-backend-test.herokuapp.com/oauth2/authorization/google', {observe : 'response'}).subscribe(res => console.log(res));
  }
}
